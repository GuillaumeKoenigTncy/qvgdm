/**
* Edits the number prototype to allow money formatting
*
* @param fixed the number to fix the decimal at. Default 2.
* @param decimalDelim the string to deliminate the non-decimal
*        parts of the number and the decimal parts with. Default "."
* @param breakdDelim the string to deliminate the non-decimal
*        parts of the number with. Default ","
* @return returns this number as a USD-money-formatted String
*		  like this: x,xxx.xx
*/
Number.prototype.money = function(fixed, decimalDelim, breakDelim){
	var n = this,
	fixed = isNaN(fixed = Math.abs(fixed)) ? 2 : fixed,
	decimalDelim = decimalDelim == undefined ? "." : decimalDelim,
	breakDelim = breakDelim == undefined ? "," : breakDelim,
	negative = n < 0 ? "-" : "",
	i = parseInt(n = Math.abs(+n || 0).toFixed(fixed)) + "",
	j = (j = i.length) > 3 ? j % 3 : 0;
	return negative + (j ? i.substr(0, j) +
		 breakDelim : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + breakDelim) +
		  (fixed ? decimalDelim + Math.abs(n - i).toFixed(fixed).slice(2) : "");
}

// Audio.prototype.stop = function() {
//     this.pause();
//     this.currentTime = 0;
// };

/**
* Plays a sound via HTML5 through Audio tags on the page
*
* @require the id must be the id of an <audio> tag.
* @param id the id of the element to play
* @param loop the boolean flag to loop or not loop this sound
*/
var soundHandle;
startSound = function(id, loop, x, lvl) {

	finaltone = document.getElementById('final');
	breaktone = document.getElementById('break');
	phonetone = document.getElementById('phone');

	// <audio id="background" src='sound/background.mp3'></audio>
	// <audio id="rightsound" src='sound/right.mp3'></audio>
	// <audio id="wrongsound" src='sound/wrong.mp3'></audio>
	// <audio id="1001000" src='sound/1001000.mp3'></audio>
	// <audio id="200032000" src='sound/200032000.mp3'></audio>
	// <audio id="64000" src='sound/64000.mp3'></audio>
	// <audio id="125000250000" src='sound/125000250000.mp3'></audio>
	// <audio id="5000000" src='sound/5000000.mp3'></audio>
	// <audio id="1000000" src='sound/1000000.mp3'></audio>

	// <audio id="phone" src='sound/phone.mp3'></audio>
	// <audio id="play" src='sound/play.mp3'></audio>
	// <audio id="break" src='sound/break.mp3'></audio>

	if(id == "200032000"){
		previous = document.getElementById("1001000");
		previous.pause();
	}
	if(id == "64000"){
		previous = document.getElementById("200032000");
		previous.pause();
	}
	if(id == "125000250000"){
		previous = document.getElementById("64000");
		previous.pause();
	}
	if(id == "5000000"){
		previous = document.getElementById("125000250000");
		previous.pause();
	}
	if(id == "1000000"){
		previous = document.getElementById("5000000");
		previous.pause();
	}

	if(lvl==1 || lvl==2 || lvl==3 || lvl==4 || lvl==5 || lvl==6){
		previous = document.getElementById("1001000");
		previous.currentTime = 0;
		if(x == 97){
			previous.pause();
			finaltone.play();
		}else if(x == 122){ 
			finaltone.pause();
			finaltone.currentTime = 0;
			finaltone = document.getElementById('final');
			previous.play();
		}else if(x == 113){
			previous.pause();
			breaktone.play();
		}else if(x == 115){ 
			breaktone.pause();
			breaktone.currentTime = 0;
			breaktone = document.getElementById('break');
			previous.play();
		}else if(x == 119){
			previous.pause();
			phonetone.play();
		}else if(x == 120){ 
			phonetone.pause();
			phonetone.currentTime = 0;
			phonetone = document.getElementById('phone');
			previous.play();
		}
	}
	
	else if(lvl==7 || lvl==8 || lvl==9 || lvl==10 || lvl==11){
		previous = document.getElementById("200032000");
		previous.currentTime = 0;
		if(x == 97){
			previous.pause();
			finaltone.play();
		}else if(x == 122){ 
			finaltone.pause();
			finaltone.currentTime = 0;
			finaltone = document.getElementById('final');
			previous.play();
		}else if(x == 113){
			previous.pause();
			breaktone.play();
		}else if(x == 115){ 
			breaktone.pause();
			breaktone.currentTime = 0;
			breaktone = document.getElementById('break');
			previous.play();
		}else if(x == 119){
			previous.pause();
			phonetone.play();
		}else if(x == 120){ 
			phonetone.pause();
			phonetone.currentTime = 0;
			phonetone = document.getElementById('phone');
			previous.play();
		}
	}


	else if(lvl==12){
		previous = document.getElementById("64000");
		previous.currentTime = 0;
		if(x == 97){
			previous.pause();
			finaltone.play();
		}else if(x == 122){ 
			finaltone.pause();
			finaltone.currentTime = 0;
			finaltone = document.getElementById('final');
			previous.play();
		}else if(x == 113){
			previous.pause();
			breaktone.play();
		}else if(x == 115){ 
			breaktone.pause();
			breaktone.currentTime = 0;
			breaktone = document.getElementById('break');
			previous.play();
		}else if(x == 119){
			previous.pause();
			phonetone.play();
		}else if(x == 120){ 
			phonetone.pause();
			phonetone.currentTime = 0;
			phonetone = document.getElementById('phone');
			previous.play();
		}
	}
	
	else if(lvl==13 || lvl==14){
		previous = document.getElementById("125000250000");
		previous.currentTime = 0;
		if(x == 97){
			previous.pause();
			finaltone.play();
		}else if(x == 122){ 
			finaltone.pause();
			finaltone.currentTime = 0;
			finaltone = document.getElementById('final');
			previous.play();
		}else if(x == 113){
			previous.pause();
			breaktone.play();
		}else if(x == 115){ 
			breaktone.pause();
			breaktone.currentTime = 0;
			breaktone = document.getElementById('break');
			previous.play();
		}else if(x == 119){
			previous.pause();
			phonetone.play();
		}else if(x == 120){ 
			phonetone.pause();
			phonetone.currentTime = 0;
			phonetone = document.getElementById('phone');
			previous.play();
		}
	}
	
	else if(lvl==15){
		previous = document.getElementById("5000000");
		previous.currentTime = 0;
		if(x == 97){
			previous.pause();
			finaltone.play();
		}
		if(x == 122){ 
			finaltone.pause();
			finaltone.currentTime = 0;
			finaltone = document.getElementById('final');
			previous.play();
		}
		if(x == 113){
			previous.pause();
			breaktone.play();
		}
		if(x == 115){ 
			breaktone.pause();
			breaktone.currentTime = 0;
			breaktone = document.getElementById('break');
			previous.play();
		}
		if(x == 119){
			previous.pause();
			phonetone.play();
		}
		if(x == 120){ 
			phonetone.pause();
			phonetone.currentTime = 0;
			phonetone = document.getElementById('phone');
			previous.play();
		}
	}
	
	else if(lvl==16){
		previous = document.getElementById("1000000");
		previous.currentTime = 0;
		if(x == 97){
			previous.pause();
			finaltone.play();
		}
		if(x == 122){ 
			finaltone.pause();
			finaltone.currentTime = 0;
			finaltone = document.getElementById('final');
			previous.play();
		}
		if(x == 113){
			previous.pause();
			breaktone.play();
		}
		if(x == 115){ 
			breaktone.pause();
			breaktone.currentTime = 0;
			breaktone = document.getElementById('break');
			previous.play();
		}
		if(x == 119){
			previous.pause();
			phonetone.play();
		}
		if(x == 120){ 
			phonetone.pause();
			phonetone.currentTime = 0;
			phonetone = document.getElementById('phone');
			previous.play();
		}	
	}else{
		soundHandle = document.getElementById(id);

		if(loop)
			soundHandle.setAttribute('loop', loop);
		soundHandle.play();		
	}

}


var waitForIt = false;
/**
* The View Model that represents one game of
* Who Wants to Be a Millionaire.
*
* @param data the question bank to use
*/
var MillionaireModel = function(data) {
	var self = this;

	// The 15 questions of this game
    this.questions = data.questions;

    // A flag to keep multiple selections
    // out while transitioning levels
    this.transitioning = false;

    // The current money obtained
 	this.money = new ko.observable(0);

 	// The current level(starting at 1)
 	this.level = new ko.observable(1);

 	// The three options the user can use to
 	// attempt to answer a question (1 use each)
 	this.usedFifty = new ko.observable(false);
 	this.usedPhone = new ko.observable(false);
 	this.usedAudience = new ko.observable(false);

 	// Grabs the question text of the current question
 	self.getQuestionText = function() {
 		return self.questions[self.level() - 1].question;
 	}

 	// Gets the answer text of a specified question index (0-3)
 	// from the current question
 	self.getAnswerText = function(index) {
 		return self.questions[self.level() - 1].content[index];
 	}

 	// Uses the fifty-fifty option of the user
 	self.fifty = function(item, event) {
 		if(self.transitioning)
 			return;
 		$(event.target).fadeOut('slow');
 		var correct = this.questions[self.level() - 1].correct;
 		var first = (correct + 1) % 4;
 		var second = (first + 1) % 4;
 		if(first == 0 || second == 0) {
 			$("#answer-one").fadeOut('slow');
 		}
 		if(first == 1 || second == 1) {
 			$("#answer-two").fadeOut('slow');
 		}
 		if(first == 2 || second == 2) {
 			$("#answer-three").fadeOut('slow');
 		}
 		if(first == 3 || second == 3) {
 			$("#answer-four").fadeOut('slow');
 		}
 	}

 	// Fades out an option used if possible
 	self.fadeOutOption = function(item, event) {
 		if(self.transitioning)
 			return;
 		$(event.target).fadeOut('slow');
 	}

 	// Attempts to answer the question with the specified
 	// answer index (0-3) from a click event of elm
 	self.answerQuestion = function(index, elm) {
 		if(self.transitioning)
 			return;
		self.transitioning = true;

 		if(self.questions[self.level() - 1].correct == index) {
 			self.rightAnswer(elm);
 		} else {
 			self.wrongAnswer(elm, self.questions[self.level() - 1].correct);
 		}
 	}

 	// Executes the proceedure of a correct answer guess, moving
 	// the player to the next level (or winning the game if all
 	// levels have been completed)
 	self.rightAnswer = function(elm) {

 		$("#" + elm).slideUp(0, function() {
 			
 			$("#" + elm).css('background', 'orange').slideDown(0, function() {

				$(document).unbind('keypress').bind('keypress').keypress(function(event){

					var x = event.which || event.keyCode;
					if(x == 111){ // o = 111 = Orange
						$("#" + elm).css('background', 'green');
						startSound('rightsound', false);
					}
					if(x == 32){ // space = 32
						self.money($(".active").data('amt'));
						if(self.level() + 1 > 15) {
							$("#game").fadeOut(0, function() {
								$("#game-over").html('You Win!');
								$("#game-over").fadeIn(0);
							});
						} else {
							self.level(self.level() + 1);
							$("#" + elm).css('background', 'rgb(50,41,70)');
							$("#answer-one").show();
							$("#answer-two").show();
							$("#answer-three").show();
							$("#answer-four").show();
							self.transitioning = false;
						}
						if(self.level() + 1 === 7){
							startSound('200032000', true);
						}
						if(self.level() + 1 === 12){
							startSound('64000', true);
						}
						if(self.level() + 1 === 13){
							startSound('125000250000', true);
						}
						if(self.level() + 1 === 15){
							startSound('5000000', true);
						}
						if(self.level() + 1 === 16){
							startSound('1000000', true);
						}
					}

					if(x == 97){ // a = 97
						startSound('final', true, x, self.level() + 1);
					}
					if(x == 122){ // z = 122
						startSound('final', true, x, self.level() + 1);
					}
					if(x == 113){// q = 113
						startSound('break', true, x, self.level() + 1);
					}
					if(x == 115){ // s = 115
						startSound('break', true, x, self.level() + 1);
					}
					if(x == 119){ // w = 119
						startSound('phone', true, x, self.level() + 1);
					}
					if(x == 120){ // s = 120
						startSound('phone', true, x, self.level() + 1);
					}
				});

 			});
 		});
 	}

 	// Executes the proceedure of guessing incorrectly, losing the game.
 	self.wrongAnswer = function(elm, goodOne) {
		console.log(goodOne);

 		$("#" + elm).slideUp(0, function() {
			
 			$("#" + elm).css('background', 'orange').slideDown(0, function() {
				$(document).unbind('keypress').bind('keypress').keypress(function(){

					var x = event.which || event.keyCode;
					if(x == 111){ // o
						if(goodOne == 0){
							$("#answer-one").css('background', 'green');
						}
						if(goodOne == 1){
							$("#answer-two").css('background', 'green');
						}
						if(goodOne == 2){
							$("#answer-three").css('background', 'green');
						}
						if(goodOne == 3){
							$("#answer-four").css('background', 'green');
						}
						$("#" + elm).css('background', 'red');
						startSound('wrongsound', false); 
					}

					if(x == 32){ // space
						$("#game").fadeOut('slow', function() {
							$("#game-over").html('Game Over!');
							$("#game-over").fadeIn('slow');

							self.transitioning = false;
						});
					}
				});
 			});
 		});
 	}

 	// Gets the money formatted string of the current won amount of money.
 	self.formatMoney = function() {
	    return self.money().money(2, '.', ',');
	}
};

// Executes on page load, bootstrapping
// the start game functionality to trigger a game model
// being created
$(document).ready(function() {
	$(document).unbind('keypress').bind('keypress').keypress(function(event){

		var x = event.which || event.keyCode;
		
		console.log(x);

		if(x == 116){ // t = 116
			startSound('theme', false, false);
		}
	});
	$.getJSON("questions.json", function(data) {
		for(var i = 1; i <= data.games.length; i++) {
			$("#problem-set").append('<option value="' + i + '">' + i + '</option>');
		}
		$("#pre-start").show();
		$("#start").click(function() {
			var index = $('#problem-set').find(":selected").val() - 1;
			ko.applyBindings(new MillionaireModel(data.games[index]));
			$("#pre-start").fadeOut('slow', function() {
				startSound('1001000', true, false);
				$("#game").fadeIn('slow');
			});
		});
	});
});

